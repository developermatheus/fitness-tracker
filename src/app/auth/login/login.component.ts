import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService) {
    this.loginForm =  this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      agree: ['', Validators.required],
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    console.log("objeto do form: ",this.loginForm);
    console.log("valor: ", this.loginForm.value);
    this.authService.login({
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    });
  }

}
