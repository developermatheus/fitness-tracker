import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'app-stop-training',
    template: `<h1 mat-dialog-title>Tem certeza fi de rapariga?</h1>
                <mat-dialog-content>
                    <p>Você já tem {{ passedData.progress }}%</p>
                </mat-dialog-content>
                <mat-dialog-actions>
                <button mat-button [mat-dialog-close]="true">SIM</button>
                <button mat-button [mat-dialog-close]="false">NAO</button>
                </mat-dialog-actions>` ,
})
export class StopTrainingComponent {
    constructor(@Inject(MAT_DIALOG_DATA) public passedData: any) {

    }
}